% Testes automatizados

%!test
%! [ind, x, d] = simplex([1 1], [5], [0; -1], 1, 2)
%! assert(ind, 0)
%! assert(x, [0; 5])

%!test
%! [ind, x, d] = simplex([1 -1 1 0; 1 1 0 1], [2; 6], [-2; -1; 0; 0], 2, 4)
%! assert(ind, 0)
%! assert(x, [4; 2; 0; 0])

%!test
%! [ind, x, d] = simplex([0 1], [2], [-1; 0], 1, 2)
%! assert(ind, -1)
%! assert(d, [1; 0])

%!test
%! [ind, x, d] = simplex([-1 1], [2], [1; 0], 1, 2)
%! assert(ind, 0)
%! assert(x, [0; 2])

%!test
%! [ind, x, d] = simplex([1 2 0 1 0; 0 1 1 1 0], [10; 3], [4; 5; 1; -1; -1], 2, 5)
%! assert(ind, -1)
%! assert(d, [0; 0; 0; 0; 1])

%!test
%! [ind, x, d] = simplex([1 1 1; 1 1 1], [1; 2], [1; 1; 1], 2, 3)
%! assert(ind, 1)

%!test
%! [ind, x, d] = simplex([1 2 3 0; -1 2 6 0; 0 4 9 0; 0 0 3 1; 2 4 6 0], [3; 2; 5; 1; 6], [1; 1; 1; 0], 5, 4)
%! assert(ind, 0)
%! assert (x, [1/2; 5/4; 0; 1])

%!test
%! [ind, x, d] = simplex([1 10; 2 5], [10; 1], [1; 1], 2, 2)
%! assert(ind, 1)

%!test
%! [ind, x, d] = simplex([1 3 0 4 1; 1 2 0 -3 1; -1 -4 3 0 0], [2; 2; 1], [2; 3; 3; 1; -2], 3, 5)
%! assert(ind, 0)
%! assert(x, [0; 0; 1/3; 0; 2], tol=e-15)
