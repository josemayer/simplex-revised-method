function [retind retv bind Binv] = simplexIteration(A,b,c,m,n,x,bind,Binv)
	% Verificações de dimensão
	assert(size(A), [m, n])
	assert(size(b), [m, 1])
	assert(size(c), [n, 1])
	assert(size(x), [n, 1])
	assert(length(bind), m)
	assert(size(Binv), [m, m])

	c = transpose(c);

	iter = 0;
	while true;
		printf("Iterando %d\n", iter);
		for i = bind
			printf("%d %.5f\n", i, x(i));
		end

		printf("\nValor da função objetivo: %.5f\n\n", c(bind) * x(bind));
		iter += 1;

		nbind = setdiff(1:n, bind);

		pT = c(bind) * Binv;
		cbar = c(nbind) - pT * A(:,nbind);

		% Procura o primeiro custo reduzido negativo
		ind = 0;
		printf("Custos reduzidos\n");
		for i = 1:(n-m)
			printf("%d %.5f\n", nbind(i), cbar(i));
			if ind == 0 && cbar(i) < 0
				ind = i;
				break
			end
		end
		printf("\n");

		% x é soluçáo ótima
		if ind == 0
			printf("Solução ótima encontrada com custo %.5f:\n", c(bind)*x(bind));
			for i = 1:n
				printf("%d %.5f\n", i, x(i));
			end
			printf("\n");
			retind = 0;
			retv = x;
			return
		end

		j = nbind(ind);

		printf("Entra na base: %d\n\n", j);

		% Computa se é limitado na j-ésima direcao escolhida
		u = Binv * A(:,j);

		positivos = [];
		printf("Direção\n");
		for i = 1:m
			printf("%d %.5f\n", bind(i), u(i));

			if u(i) > 0
				positivos = [positivos, i];
			end
		end

		printf("\n");

		% Possivelmente o poliedro não é limitado na j-ésima direção
		if length(positivos) == 0
			retind = -1;
			retv = zeros(n, 1);
			retv(bind) = -u;
			retv(j) = 1;

			printf("Direção que leva a custo menos infinito encontrada:\n");
			for i = 1:n
				printf("%d %.5f\n", i, retv(i));
			end
			printf("\n");

			return
		end

		% Calcula theta_estrela e o índice l de substituição
		theta = @(i) x(bind(i))/u(i);
		[theta_star, l] = argmin(positivos, theta);
		printf("Theta*\n%.5f\n\nSai da base: %d\n\n", theta_star, bind(l));

    [bind Binv x] = pivot(A, bind, Binv, l, j, m, x, theta_star);
	end
end

function [fm, m] = argmin(v, f)
	fm = Inf;
	m = 0;

	for i = v
		f_i = f(i);
		if (fm > f_i)
			fm = f_i;
			m = i;
		end
	end

	return
end
