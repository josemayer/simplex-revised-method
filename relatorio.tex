\documentclass{article}
\usepackage{xcolor}
\usepackage[utf8]{inputenc}
\usepackage{algpseudocode, algorithm}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{tikz}
\usepackage{pgfplots}

\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}
\pgfplotsset{compat=1.18}

% Declaracoes em Português
\algrenewcommand\algorithmicend{\textbf{fim}}
\algrenewcommand\algorithmicdo{\textbf{faça}}
\algrenewcommand\algorithmicwhile{\textbf{enquanto}}
\algrenewcommand\algorithmicfor{\textbf{para}}
\algrenewcommand\algorithmicif{\textbf{se}}
\algrenewcommand\algorithmicthen{\textbf{então}}
\algrenewcommand\algorithmicelse{\textbf{senão}}
\algrenewcommand\algorithmicreturn{\textbf{devolve}}
\algrenewcommand\algorithmicfunction{\textbf{função}}

% Renomeia a seção de algoritmo
\makeatletter
\renewcommand{\ALG@name}{Algoritmo}

% Rearranja os finais de cada estrutura
\algrenewtext{EndWhile}{\algorithmicend\ \algorithmicwhile}
\algrenewtext{EndFor}{\algorithmicend\ \algorithmicfor}
\algrenewtext{EndIf}{\algorithmicend\ \algorithmicif}
\algrenewtext{EndFunction}{\algorithmicend\ \algorithmicfunction}

\algnewcommand\algorithmicto{\textbf{até}}
\algrenewtext{For}[3]%
{\algorithmicfor\ #1 $\gets$ #2 \algorithmicto\ #3 \algorithmicdo}

% Comandos auxiliares para plotagem de solução gráfica
\usepgfplotslibrary{fillbetween}
\usetikzlibrary{patterns}

\makeatletter
\newcommand{\pgfplotsdrawaxis}{\pgfplots@draw@axis}
\makeatother
\pgfplotsset{only axis on top/.style={axis on top=false, after end axis/.code={
             \pgfplotsset{axis line style=opaque, ticklabel style=opaque, tick style=opaque,
                          grid=none}\pgfplotsdrawaxis}}}

\newcommand{\drawge}{-- (rel axis cs:1,0) -- (rel axis cs:1,1) -- (rel axis cs:0,1) \closedcycle}
\newcommand{\drawle}{-- (rel axis cs:1,1) -- (rel axis cs:1,0) -- (rel axis cs:0,0) \closedcycle}

% Possibilita espaçamento vertical na bmatrix
\makeatletter
\renewcommand*\env@matrix[1][\arraystretch]{%
  \edef\arraystretch{#1}%
  \hskip -\arraycolsep
  \let\@ifnextchar\new@ifnextchar
  \array{*\c@MaxMatrixCols c}}
\makeatother

\title{Otimização Linear (MAC0315) \\ Relatório - EP 2}
\author{José Lucas Silva Mayer \\ n.º USP: 11819208 \\ \\ Maximilian Cabrajac Goritz \\ n.º USP: 11795418}
\date{Outubro, 2022.}

\begin{document}

\maketitle

\section{Introdução}

O método simplex é utilizado para resolver problemas de programação linear na forma padrão, ou seja, que podem ser definidos por uma matriz $A \in \mathbb{R}^{m \times n}$ e por vetores $b \in \mathbb{R}^m$ e $c \in \mathbb{R}^n$ onde $m, n \in \mathbb{Z}$ e $0 \leq m \leq n$, de forma que $Ax = b$ representa as restrições que não são da forma $x \geq 0$ e $c$ representa a função de custos $c^T x$.

O método simplex se baseia na exploração de soluções viáveis básicas.
A partir de uma base $B(1), ..., B(m)$, que gera uma solução viável básica, é feita a substituição de um dos índices da base no decorrer de uma iteração do método, gerando uma nova base $\overline{B}(1), ..., \overline{B}(m)$, cuja solução viável básica correspondente tem custo menor (ou igual quando há soluções degeneradas, o que não é tratado neste EP).

Dentre as possíveis implementações do métodos simplex implementamos o método simplex revisado, que mantém durante suas iterações a inversa da matriz básica ($B^{-1}$). Este algoritmo é mais eficiente que uma implementação ingênua do método, uma vez que não calcula $B^{-1}$ a cada iteração, o que tomaria tempo $\text{O}(m^3)$.

Em anexo a este relatório, há o código-fonte da segunda fase da implementação em octave do método simplex \textminus no arquivo \texttt{simplex.m} \textminus e uma bateria de testes \textminus no arquivo \texttt{teste.m}.

\newpage

\section{Pseudocódigo}

\begin{algorithm}
\caption{Segunda fase do método Simplex Revisado}
\begin{algorithmic}[1]
  \Function{Simplex}{$A$, $b$, $c$, $m$, $n$, $x$, $b_{ind}$, $B^{-1}$}
  \While {Verdadeiro}
    \State $c_{B}$ $\gets$ $c_j$, para todo $j \in b_{ind}$
    \State $p^T$ $\gets$ $c_{B}^T B^{-1}$

    \State \textbf{calcula} $\overline{c_{j}}$ $\gets$ $c_{j} - p^{T}A_{j}$, para todo $j \not \in b_{ind}$

    \If {não existe um $\overline{c_{j}}$ negativo}
      \State \Return a solução ótima $x$
    \EndIf

    \State \textbf{escolhe} o menor $j$ tal que $c_{j} < 0$
    \State \textbf{calcula} $u = B^{-1} A_{j}$
    \If {nenhum componente de $u$ é positivo}
      \State o custo ótimo é -$\infty$; \Return a $j$-ésima direção básica
    \EndIf

    \State \textbf{calcula} $\theta^{*} = \min_{\{i = 1, \cdots, m | u_{i} > 0\}}{\frac{x_{B(i)}}{u_{i}}}$
    \State \textbf{escolhe} $l$ como o $i$ que realiza o mínimo de $\theta^{*}$
    \State \textbf{atualiza} $B^{-1}$ conforme a substituição da $l$-ésima coluna por $A_{j}$
    \State \textbf{atualiza} $x$ com a solução básica correspondente a nova base
  \EndWhile
\EndFunction
\end{algorithmic}
\end{algorithm}

\section{Funcionamento do Algorimo em \texttt{simplex.m}}
O algoritmo implementado recebe, de ínicio, a matriz $A$ (\texttt{A}), o vetor $b$ (\texttt{b}), o vetor de custos $c$ (\texttt{c}), as variáveis de dimensionalidade $m$ (\texttt{m}) e $n$ (\texttt{n}) e a representação da solução básica inicial, por meio da base $b_{ind}$ (\texttt{bind}) e sua solução corresponte $x$ (\texttt{x}). Por fim, recebe a inversa da matriz básica inicial $B^{-1}$ (\texttt{Binv}) que será mantida pelo algoritmo.

A cada iteração, o algoritmo do simplex revisado (implementado no arquivo \texttt{simplex.m}) executa os seguintes passos:

\begin{itemize}
  \item Linha \texttt{23}: Calcula os indices das variáveis não básicas e armazena em \texttt{nbind}
  \item Linhas \texttt{25-26}: Calcula os custos reduzidos das variaveis não básicas e o armazena no vetor \texttt{cbar}.
  \item Linhas \texttt{29-37}: Percorre o vetor \texttt{cbar} e seleciona o primeiro índice de direção \texttt{ind} cujo custo reduzido é negativo.
  \item Linhas \texttt{41-50}: Se nenhum custo reduzido negativo foi encontrado, temos que a solução corrente é ótima, assim o algoritmo termina devolvendo o vetor \texttt{[0, x]}.
  \item Linha \texttt{52}. Armazena em \texttt{j} o índice da variável que entrará na base nesta iteração, como escolhido anteriormente.
  \item Linha \texttt{57}. Calcula o oposto das componentes básicas da \texttt{j}-ésima direção básica e armazena em \texttt{u}.
  \item Linhas \texttt{59-67}. Armazena em \texttt{positivos} os índices para quais \texttt{u} é positivo.
  \item Linhas \texttt{72-85}. Se não existem componentes positivas de \texttt{u}, então encontramos uma direção em que o poliedro não é limitado e, como calculado anteriormente, temos que o custo reduzido é negativo, ou seja, esta direção leva a soluções arbitrariamente pequenas. Nesse caso, construímos o vetor de direção $d$ atribuindo 0 a todo $d_{i}$ tal que $i$ é não básico e $i \neq j$, 1 à $j$-ésima componente de $d$ ($d_{j}$ = 1) e os valores de $-u$ aos $d_{B(i)}$ correspondentes, para os índices básicos $i$. Assim, o algoritmo termina retornando o vetor \texttt{[-1, d]}
  \item Linhas \texttt{88-89}. Calcula o maior passo viável \texttt{theta\_star} e o índice \texttt{l} da variável que entrará na base com tal passo.
  \item Linhas \texttt{93-99}. Computa a atualização de \texttt{B\_inv} de acordo com a substituição da \texttt{l}-ésima coluna da base por $A_{j}$, subtraindo $\frac{u_{i}}{u_{l}} \cdot \texttt{B\_inv(j, :)}$ para cada linha $i \neq l$ e dividindo a $l$-ésima linha por $u_{l}$.
  \item Linhas \texttt{102-103}. Armazena em $x$ a nova solução corrente.
  \item Linha \texttt{105}. Atualiza o vetor de índices \texttt{bind} com o novo índice básico \texttt{j} na \texttt{l}-ésima posição.
\end{itemize}

\section{Testes}

\subsection{Problema 1}

Seja o problema de programação linear definido por:

\begin{alignat*}{4}
  	\text{Minimizar } && && -x_{2} &&\\
  	\text{Sujeito a } && x_{1} && + x_{2} && = 5 \\
  	&& x_{1},&&x_{2} &&\geq 0
\end{alignat*}

O problema está no formato padrão. Partindo da solução viável básica inicial $x^T = \begin{bmatrix}5 & 0\end{bmatrix}$, temos a matriz básica $B = \begin{bmatrix}1\end{bmatrix}$ (e, por conseguinte, $B^{-1} = \begin{bmatrix}1\end{bmatrix}$), $b_{ind} = \begin{bmatrix}1\end{bmatrix}$, a matriz $A = \begin{bmatrix}1 & 1\end{bmatrix}$, o vetor $b = \begin{bmatrix}5\end{bmatrix}$ e o vetor de custos $c = \begin{bmatrix}0 & -1\end{bmatrix}$. Vejamos a simulação das iterações do algoritmo simplex.

\begin{itemize}
  \item \textbf{Iteração 1}. O custo calculado para o ponto $x$ é de 0, já que $x_{2} = 0$. Calculando $p^{T}$:

    \begin{align*}
      p^{T} & = c_{B} B^{-1} \\
            & = \begin{bmatrix}0\end{bmatrix}\begin{bmatrix}1\end{bmatrix} \\
            & = \begin{bmatrix}0\end{bmatrix}
    \end{align*}

    Calculando os custos reduzidos para as variáveis não básicas temos, considerando que $x_{2}$ é a única não básica:

    \begin{align*}
      \overline{c_{2}} & = c_{2} - \begin{bmatrix}0\end{bmatrix}A_{2} \\
                       & = -1 - \begin{bmatrix}0\end{bmatrix}\begin{bmatrix}1\end{bmatrix} \\
                       & = - 1 - 0 \\
                       & = -1
    \end{align*}

    Como o primeiro e único custo é negativo, tomamos \texttt{ind = 1} e, consequentemente, \texttt{j = 2}, já que 2 é o primeiro e único índice dos índices não básicos (\texttt{nbind(ind) = 2}). Calculando, após isso, o valor de $u$, obtemos:

    \begin{align*}
      u & = B^{-1}A_{2} \\
        & = \begin{bmatrix}1\end{bmatrix}\begin{bmatrix}1\end{bmatrix} \\
        & = \begin{bmatrix}1\end{bmatrix}
    \end{align*}

    Como existe uma componente positiva em $u$, o custo ótimo não é $-\infty$. Além disso, $u_{1}$ é o único componente positivo, então $\theta^{*} = \frac{x_{B(1)}}{u_{1}} = \frac{5}{1} = 5$. Nesse caso, o valor de $l$ é igual a 1. Como substituímos a 1ª coluna da base pela coluna $A_{2}$, então precisamos atualizar $B^{-1}$. Como se trata de uma matriz de apenas uma linha e $l = 1$ (a única linha é a l-ésima linha), então dividimos essa linha por $u_{l} = 1$.

    \begin{align*}
      B^{-1} & = \begin{bmatrix}1\end{bmatrix} \\
             & = \begin{bmatrix}\frac{1}{1}\end{bmatrix} \\
             & = \begin{bmatrix}1\end{bmatrix}
    \end{align*}

    Atualizando a solução viável básica $x$, subtraímos $\theta^{*} u_{1}$ de $x_{1}$ e atribuímos $\theta^{*}$ a $x_{2}$:

    \begin{align*}
      x & = \begin{bmatrix}5 - \theta^{*}u_{1} \\ \theta^{*} \end{bmatrix} \\
        & = \begin{bmatrix}5 - 5 \cdot 1 \\ 5 \end{bmatrix} \\
        & = \begin{bmatrix}0 \\ 5\end{bmatrix}
    \end{align*}

    Por fim, substituímos o $l$-ésimo termo do vetor de índices básicos \texttt{bind} por $j$, isto é, \texttt{bind[1] = 2}, por conseguinte, \texttt{bind = [2]}.

  \item \textbf{Iteração 2}. O custo calculado para o ponto $x$ é de -5, já que $x_{2} = -5$. Calculando $p^{T}$:

    \begin{align*}
      p^{T} & = c_{B} B^{-1} \\
            & = \begin{bmatrix}-1\end{bmatrix}\begin{bmatrix}1\end{bmatrix} \\
            & = \begin{bmatrix}-1\end{bmatrix}
    \end{align*}

    Calculando os custos reduzidos para as variáveis não básicas temos, considerando que $x_{1}$ é a única não básica:

    \begin{align*}
      \overline{c_{1}} & = c_{1} - \begin{bmatrix}-1\end{bmatrix}A_{2} \\
                       & = 0 - \begin{bmatrix}-1\end{bmatrix}\begin{bmatrix}1\end{bmatrix} \\
                       & = 0 - (-1) \\
                       & = 1
    \end{align*}

    Como $\overline{c_{1}}$ é o único custo reduzido e ele é positivo, então a solução corrente é ótima. Nesse caso, retorna o vetor $\left[0, x\right] = \left[0, \begin{bmatrix}0 \\ 5\end{bmatrix}\right]$ como solução.
\end{itemize}

Vejamos a representação e resolução gráfica desse problema, com a solução ótima marcada em vermelho e a solução inicial em roxo:


\begin{center}
  \begin{tikzpicture}
    \begin{axis}[smooth,
      axis line style=very thick,
      axis x line=bottom,
      axis y line=left,
      grid=major,
      grid style={line width=.1pt, draw=gray!30},
      ymin=-1.0,ymax=5.5,xmin=-0.2,xmax=5.5,
      xlabel=$x_1$, ylabel=$x_2$,
      xtick={0, 1, 2, 3, 4, 5},
      ytick={0, 1, 2, 3, 4, 5},
      axis lines=middle]
      \addplot[thick,domain=-0.2:5.5] {5 - x};
      \node (a) at (0, 5) {};
      \node (b) at (5, 0) {};
      \addplot[domain=-0.2:5.5,dashed,purple] {5};
      \addplot[domain=-0.2:5.5,dashed,purple] {3};
      \addplot[domain=-0.2:5.5,dashed,purple] {1};
      \node[circle, red, fill=purple,scale=0.5] at (0, 5) {};
      \node[circle, yellow, fill=violet,scale=0.5] at (5, 0) {};
      \draw[line width=2pt,teal,-stealth](0,0)--(0,-1) node[anchor=south west]{$\boldsymbol{c}$};
      \draw[line width=2pt,purple,-stealth](0,0)--(0,1) node[anchor=north west]{$\boldsymbol{-c}$};
    \end{axis}
  \end{tikzpicture}
\end{center}

Como, de fato, a solução ótima é $\begin{bmatrix}0 \\ 5\end{bmatrix}$, temos a confirmação do resultado obtido pelo simplex revisado.

\subsection{Problema 2}

Seja o problema de programação linear definido por:


\begin{alignat*}{4}
  	\text{Minimizar } && -x_{1} && &&\\
  	\text{sujeito a } && && x_{2} && = 2 \\
  	&& x_{1}, && x_{2} && \geq 0
\end{alignat*}

O problema está no formato padrão. Partindo-se da solução viável básica inicial $x^T = \begin{bmatrix}0 & 2\end{bmatrix}$, temos a matriz básica $B = \begin{bmatrix}1\end{bmatrix}$ (e, por conseguinte, $B^{-1} = \begin{bmatrix}1\end{bmatrix}$), $b_{ind} = \begin{bmatrix}2\end{bmatrix}$, a matriz $A = \begin{bmatrix}0 & 1\end{bmatrix}$, o vetor $b = \begin{bmatrix}2\end{bmatrix}$ e o vetor de custos $c = \begin{bmatrix}-1 & 0\end{bmatrix}$. Vejamos a simulação de passos implementados pelo algoritmo em \texttt{simplex.m}:

\begin{itemize}
  \item \textbf{Iteração 1}. O custo calculado no ponto $x$ é de 0, já que $x_{1} = 0$. Calculando $p^{T}$:

    \begin{align*}
      p^{T} & = c_{B} B^{-1} \\
            & = \begin{bmatrix}0\end{bmatrix} \begin{bmatrix}1\end{bmatrix} \\
            & = \begin{bmatrix}0\end{bmatrix}
    \end{align*}

    Como a única variável não básica é $x_{1}$, vamos calcular o valor de seu custo reduzido:

    \begin{align*}
      \overline{c_{1}} & = c_{1} - p^{T}A_{1} \\
                       & = -1 - \begin{bmatrix}0\end{bmatrix}\begin{bmatrix}0\end{bmatrix} \\
                       & = -1 - 0 \\
                       & = -1
    \end{align*}

    Como seu valor é negativo, tomamos \texttt{ind = 1} e, consequentemente, \texttt{j = 1}, uma vez que \texttt{nbind(ind) = 1}. Em sequência, calculando o valor de $u$, obtemos:

    \begin{align*}
      u & = B^{-1} A_{1} \\
        & = \begin{bmatrix}1\end{bmatrix}\begin{bmatrix}0\end{bmatrix} \\
        & = \begin{bmatrix}0\end{bmatrix}
    \end{align*}

    Como $u$ não tem nenhum valor positivo (seu único valor é $u_{1} = 0$), então a direção viável escolhida permite construir soluções arbitrariamente pequenas. Sabemos então que o custo ótimo é de $-\infty$. Nesse caso, construimos o vetor de direção $d$ atribuindo $-u_{1}$ ao único índice básico (2) e 1 ao $j$-ésimo (primeiro) índice escolhido. Então, por fim, retornamos o vetor $\left[-1, d\right] = \left[-1, \begin{bmatrix}1 \\ -u_{1}\end{bmatrix}\right] = \left[-1, \begin{bmatrix}1 \\ 0\end{bmatrix}\right]$.
\end{itemize}

Vejamos a representação e resolução gráfica desse problema, com a solução inicial apresentada em roxo:

\begin{center}
  \begin{tikzpicture}
    \begin{axis}[smooth,
      axis line style=very thick,
      axis x line=bottom,
      axis y line=left,
      grid=major,
      grid style={line width=.1pt, draw=gray!30},
      ymin=-0.2,ymax=5.5,xmin=-1.0,xmax=5.5,
      xlabel=$x_1$, ylabel=$x_2$,
      xtick={0, 1, 2, 3, 4, 5},
      ytick={0, 1, 2, 3, 4, 5},
      axis lines=middle]
      \addplot[thick,domain=-0.2:5.5] {2};
      \node (a) at (0, 5) {};
      \node (b) at (5, 0) {};
      \addplot[domain=-0.2:6,dashed,purple] coordinates {(1,0)(1,6)};
      \addplot[domain=-0.2:6,dashed,purple] coordinates {(3,0)(3,6)};
      \addplot[domain=-0.2:6,dashed,purple] coordinates {(5,0)(5,6)};
      \draw[line width=2pt,teal,-stealth](0,0)--(-1,0) node[anchor=south west]{$\boldsymbol{c}$};
      \draw[line width=2pt,purple,-stealth](0,0)--(1,0) node[anchor=south east]{$\boldsymbol{-c}$};
      \draw[line width=2pt,gray,-stealth](0,2)--(1,2) node[anchor=south east]{$\boldsymbol{d}$};
      \node[circle, yellow, fill=violet,scale=0.5] at (0, 2) {};
    \end{axis}
  \end{tikzpicture}
\end{center}

Como, de fato, o custo ótimo é $-\infty$, já que o poliedro é ilimitado na direção $-c$, temos a confirmação do resultado obtido pelo simplex revisado.

\subsection{Problema 3}

Seja o problema de programação linear definido por:


\begin{alignat*}{4}
  	\text{Minimizar }  && -2x_{1} && - x_{2}\\
  	\text{sujeito a } && x_{1} && - x_{2} && \leq 2 \\
  	&& x_{1} && + x_{2} && \leq 6 \\
  	&& x_{1}, && x_{2} && \geq 0
\end{alignat*}

Transformando-o em um problema no formato padrão, introduzindo as variáveis de folga $x_{3}$ e $x_{4}$, temos:

\begin{alignat*}{6}
  	\text{Minimizar }  && -2x_{1} && - x_{2}\\
  	\text{sujeito a } && x_{1} && - x_{2} && + x_{3} && && = 2 \\
  	&& x_{1} && + x_{2} && && +x_{4} && = 6 \\
  	&& x_{1}, && x_{2}, && x_{3}, && x_{4} && \geq 0
\end{alignat*}


Partindo-se da solução viável básica inicial $x^{T} = \begin{bmatrix}0 & 0 & 2 & 6\end{bmatrix}$, temos a matriz básica $B = \begin{bmatrix}1 & 0 \\ 0 & 1\end{bmatrix}$ $\left( \text{e, por conseguinte, }B^{-1} = \begin{bmatrix}1 & 0 \\ 0 & 1\end{bmatrix} \right)$, $b_{ind} = \begin{bmatrix}3 & 4\end{bmatrix}$, a matriz $A = \begin{bmatrix}1 & -1 & 1 & 0 \\ 1 & 1 & 0 & 1\end{bmatrix}$, o vetor $b = \begin{bmatrix}2 \\ 6\end{bmatrix}$ e o vetor de custos $c = \begin{bmatrix}-2 & -1 & 0 & 0\end{bmatrix}$. Vejamos a simulação de passos implementados pelo algoritmo em \texttt{simplex.m}:

\begin{itemize}
  \item \textbf{Iteração 1}. O custo calculado no ponto $x$ é de 0, já que $x_{1} = x_{2} = 0$. Calculando $p^{T}$:

    \begin{align*}
      p^{T} & = c_{B} B^{-1} \\
            & = \begin{bmatrix}0 & 0\end{bmatrix}\begin{bmatrix}1 & 0 \\ 0 & 1\end{bmatrix} \\
            & = \begin{bmatrix}0 & 0\end{bmatrix}
    \end{align*}

    Como as variáveis não básicas são $x_{1}$ e $x_{2}$, já que $b_{ind} = \begin{bmatrix}3 & 4\end{bmatrix}$, então calculamos os valores dos custos reduzidos $\overline{c_{1}}$ e $\overline{c_{2}}$, nessa ordem, parando ao encontrar o primeiro valor negativo:

    Calculando o vetor de custos reduzidos obtemos:

    \begin{align*}
      \begin{bmatrix}\overline{c_1} & \overline{c_2}\end{bmatrix} & = \begin{bmatrix}c_1 & c_2\end{bmatrix} - p^{T} \begin{bmatrix}A_1 & A_2\end{bmatrix} \\
        & = \begin{bmatrix}-2 & -1\end{bmatrix} - \begin{bmatrix}0 & 0\end{bmatrix}\begin{bmatrix}1 & -1\\ 1 & 1\end{bmatrix} \\
        & = \begin{bmatrix}-2 & -1\end{bmatrix} - \begin{bmatrix}0 & 0\end{bmatrix} \\
        & = \begin{bmatrix}-2 & -1\end{bmatrix}
    \end{align*}

    Como o primeiro custo reduzido é não negativo tem índice 1, temos que \texttt{ind = 1}, consequentemente \texttt{j = 1}, uma vez que $nbind = [1, 2]$
    Calculando as componentes não basicas da $j$-ésima direção basica escolhida temos:

  \begin{equation*}
    \begin{aligned}
      u & = B^{-1} A_{j} \\
        & = B^{-1} A_{1} \\
        & = \begin{bmatrix}1 & 0 \\ 0 & 1\end{bmatrix}\begin{bmatrix}1 \\ 1\end{bmatrix} \\
        & = \begin{bmatrix}1 \\ 1\end{bmatrix}
    \end{aligned}
  \end{equation*}

  Como existem componentes positivas em $u$, o custo ótimo não é $-\infty$. Vamos calcular o $\theta^{*}$:

  \begin{equation*}
    \begin{aligned}
      \theta^{*} & = \min{\left\{\frac{x_{B(1)}}{u_{1}}, \frac{x_{B(2)}}{u_{2}}\right\}} \\
                 & = \min{\left\{\frac{x_{3}}{u_{1}}, \frac{x_{4}}{u_{2}}\right\}} \\
                 & = \min{\left\{\frac{2}{1}, \frac{6}{1}\right\}} \\
                 & = 2
    \end{aligned}
  \end{equation*}

  Como, nesse caso, o índice que realiza o mínimo é $l = 1$, então escolhemos o 1º elemento dos índices básicos para sair da base (que é igual a 3, já que $b_{ind} = [3, 4]$). Além disso, como $A_{3}$ será substituído por $A_{1}$ na matriz básica, precisamos atualizar $B^{-1}$:

  \begin{equation*}
    \begin{aligned}
      B^{-1} & = \begin{bmatrix}[1.5]\frac{1}{u_{1}} & \frac{0}{u_{1}} \\ 0 - \frac{u_{2}}{u_{1}}B^{-1}_{11} & 1 - \frac{u_{2}}{u_{1}}B^{-1}_{12}\end{bmatrix} \\ 
             & = \begin{bmatrix}[1.5]\frac{1}{1} & \frac{0}{1} \\ 0 - \frac{1}{1} \cdot 1 & 1 - \frac{1}{1} \cdot 0\end{bmatrix} \\
             & = \begin{bmatrix}[1.5] 1 & 0 \\ -1 & 1 \end{bmatrix}
    \end{aligned}
  \end{equation*}

  Atualizando, agora, a solução viável básica $x$, subtraímos $\theta^{*}u_{1}$ de $x_{3}$ e $\theta^{*}u_{2}$ de $x_{4}$. Além disso, atribuímos $\theta^{*}$ a $x_{1}$ (solução que se torna, agora, básica).

  \begin{align*}
    \overline{x} & = \begin{bmatrix}\overline{x_{1}} \\ \overline{x_{2}} \\ \overline{x_{3}} \\ \overline{x_{4}}\end{bmatrix} \\
                 & = \begin{bmatrix} \theta^{*} \\ x_{2} \\ x_{3} - \theta^{*}u_{1} \\ x_{4} - \theta^{*}u_{2} \end{bmatrix} \\
                 & = \begin{bmatrix} 2 \\ 0 \\ 2 - 2 \cdot 1 \\ 6 - 2 \cdot 1 \end{bmatrix} \\
                 & = \begin{bmatrix} 2 \\ 0 \\ 0 \\ 4 \end{bmatrix} \\
  \end{align*}

  Assim, temos a nova solução básica $\overline{x}^{T} = \begin{bmatrix}2 & 0 & 0 & 4\end{bmatrix}$. Atualizando, por fim, o vetor de índices basicos $b_{ind}$, substituímos a 1ª componente ($l$-ésima) por $j$, que é o novo índice básico (\texttt{bind(l) = j}), tendo agora $b_{ind} = [1, 4]$.


  \item \textbf{Iteração 2}. O custo calculado no ponto $x$ é de -4, já que $x_{1} = 2$ e $x_{2} = 0$. Calculando $p^{T}$:

  \begin{equation*}
    \begin{aligned}
      p^{T} & = c_{B} B^{-1} \\
            & = \begin{bmatrix}-2 & 0\end{bmatrix}\begin{bmatrix}1 & 0 \\ -1 & 1\end{bmatrix} \\
            & = \begin{bmatrix}-2 & 0\end{bmatrix}
    \end{aligned}
  \end{equation*}

  Como as variáveis não básicas são $x_{2}$ e $x_{3}$, já que $b_{ind} = \begin{bmatrix}1 & 4\end{bmatrix}$, então calculamos os valores dos custos reduzidos $\overline{c_{2}}$ e $\overline{c_{3}}$:

  \begin{equation*}
    \begin{aligned}
      \begin{bmatrix}\overline{c_{2}} & \overline{c_{3}}\end{bmatrix} & = \begin{bmatrix}c_{2} & c_{3}\end{bmatrix} - p^{T} \begin{bmatrix}A_{2} & A_{3}\end{bmatrix} \\
                                                                       & = \begin{bmatrix} -1 & 0 \end{bmatrix} - \begin{bmatrix}-2 & 0\end{bmatrix}\begin{bmatrix}-1 & 1 \\ 1 & 0\end{bmatrix} \\
                                                                       & = \begin{bmatrix}-1 & 0\end{bmatrix} - \begin{bmatrix}2 & -2\end{bmatrix} \\
                                                                       & = \begin{bmatrix}-3 & 2\end{bmatrix}
    \end{aligned}
  \end{equation*}

  Nesse estágio, temos o valor da variável de índices não básicos \texttt{nbind = [2, 3]}. Como $\overline{c_{2}}$ é negativo e 2 é o primeiro índice dos não básicos, escolhemos \texttt{ind = 1} e, consequentemente, $j = 2$, já que \texttt{nbind(ind) = 2}. Calculando, agora, o valor de $u$, obtemos:

  \begin{equation*}
    \begin{aligned}
      u & = B^{-1} A_{2} \\
        & = \begin{bmatrix}1 & 0 \\ -1 & 1\end{bmatrix}\begin{bmatrix}-1 \\ 1\end{bmatrix} \\
        & = \begin{bmatrix}-1 \\ 2\end{bmatrix}
    \end{aligned}
  \end{equation*}

  Como existem componentes positivas em $u$, o custo ótimo não é $-\infty$. Vamos calcular o $\theta^{*}$:

  \begin{equation*}
    \begin{aligned}
      \theta^{*} & = \min{\left\{\frac{x_{B(2)}}{u_{2}}\right\}} \\
                 & = \frac{x_{4}}{u_{2}} \\
                 & = \frac{4}{2} \\
                 & = 2
    \end{aligned}
  \end{equation*}

  Como, nesse caso, o índice que realiza o mínimo é $l = 2$, então escolhemos o 2º elemento dos índices básicos para sair da base (que é igual a 4, já que $b_{ind} = [1, 4]$). Além disso, como $A_{4}$ será substituído por $A_{2}$ na matriz básica, precisamos atualizar $B^{-1}$:


  \begin{equation*}
    \begin{aligned}
      B^{-1} & = \begin{bmatrix}[1.5]1 - \frac{u_{1}}{u_{2}}B^{-1}_{21} & 0 - \frac{u_{1}}{u_{2}}B^{-1}_{22} \\ \frac{-1}{u_{2}} & \frac{1}{u_{2}}\end{bmatrix} \\ 
             & = \begin{bmatrix}[1.5]1 - \frac{-1}{2}\cdot (-1) & 0 - \frac{-1}{2} \cdot 1 \\ \frac{-1}{2} & \frac{1}{2}\end{bmatrix} \\ 
             & = \begin{bmatrix}[1.5] \frac{1}{2} & \frac{1}{2} \\ -\frac{1}{2} & \frac{1}{2} \end{bmatrix}
    \end{aligned}
  \end{equation*}

  Para atualizar a solução viável básica $x$, subtraímos $\theta^{*}u_{1}$ de $x_{1}$ e $\theta^{*}u_{2}$ de $x_{4}$. Além disso, atribuímos $\theta^{*}$ a $x_{2}$.

  \begin{align*}
    \overline{x} & = \begin{bmatrix}\overline{x_{1}} \\ \overline{x_{2}} \\ \overline{x_{3}} \\ \overline{x_{4}}\end{bmatrix} \\
                 & = \begin{bmatrix} x_{1} - \theta^{*}u_{1} \\ \theta^{*} \\ x_{3} \\ x_{4} - \theta^{*}u_{2} \end{bmatrix} \\
                 & = \begin{bmatrix} 2 - 2 \cdot (-1) \\ 2 \\ 0 \\ 4 - 2 \cdot 2 \end{bmatrix} \\
                 & = \begin{bmatrix} 4 \\ 2 \\ 0 \\ 0 \end{bmatrix} \\
  \end{align*}

  Assim, temos a nova solução básica $\overline{x}^{T} = \begin{bmatrix}4 & 2 & 0 & 0\end{bmatrix}$. Atualizando, por fim, o vetor de índices basicos $b_{ind}$, substituímos a 2ª componente ($l$-ésima) por $j$, que é o novo índice básico (\texttt{bind(l) = j}), tendo agora $b_{ind} = [1, 2]$.

  \item \textbf{Iteração 3}. O custo calculado no ponto $x$ é de -10, já que $x_{1} = 4$ e $x_{2} = 2$. Calculando $p^{T}$:

    \begin{align*}
      p^{T} & = c_{B} B^{-1} \\
            & = \begin{bmatrix}-2 & -1\end{bmatrix}\begin{bmatrix}[1.5]\frac{1}{2} & \frac{1}{2} \\ -\frac{1}{2} & \frac{1}{2} \end{bmatrix} \\
            & = \begin{bmatrix}-\frac{1}{2} & -\frac{3}{2}\end{bmatrix}
    \end{align*}

    Como as variáveis não básicas são $x_{3}$ e $x_{4}$, já que $b_{ind} = \begin{bmatrix}1 & 2\end{bmatrix}$, então calculamos os valores dos custos reduzidos $\overline{c_{3}}$ e $\overline{c_{4}}$:

    \begin{align*}
      \begin{bmatrix}\overline{c_{3}} & \overline{c_{4}}\end{bmatrix} & = \begin{bmatrix}c_{3} & c_{4}\end{bmatrix} - p^{T} \begin{bmatrix}A_{3} & A_{4}\end{bmatrix} \\
      	& = \begin{bmatrix} 0 & 0 \end{bmatrix} - \begin{bmatrix}-\frac{1}{2} & -\frac{3}{2}\end{bmatrix}\begin{bmatrix}1 & 0 \\ 0 & 1\end{bmatrix} \\
      	& = \begin{bmatrix}\frac{1}{2} & \frac{3}{2}\end{bmatrix}
    \end{align*}

    Como todos os custos reduzidos são positivos, concluímos que a solução corrente $x^T = \begin{bmatrix}4 & 2 & 0 & 0\end{bmatrix}$ é ótima e deve ser retornada. Notemos que esta solução equivale a $x^T = \begin{bmatrix}4 & 2\end{bmatrix}$ no problema inicial.
\end{itemize}

Vejamos a representação e resolução gráfica do problema original. Notemos que, nesse caso, não podemos representar a solução inicial do problema equivalente. A solução viável básica ótima está representada em vermelho.

\begin{center}
  \begin{tikzpicture}
    \begin{axis}[smooth,
      axis line style=very thick,
      axis x line=bottom,
      axis y line=left,
      grid=major,
      grid style={line width=.1pt, draw=gray!30},
      ymin=-0.5,ymax=6.5,xmin=-1.0,xmax=6.5,
      xlabel=$x_1$, ylabel=$x_2$,
      xtick={0, 1, 2, 3, 4, 5, 6},
      ytick={0, 1, 2, 3, 4, 5, 6},
      axis lines=middle]
      \addplot[thick,domain=-0.2:6.5] {x - 2};
      \addplot[thick,domain=-0.2:6.5] {6 - x};
      \node (a) at (0, 0) {};
      \node (b) at (2, 0) {};
      \node (c) at (4, 2) {};
      \node (d) at (0, 6) {};
      \filldraw[fill=black!10] (a.center) -- (b.center) -- (c.center) -- (d.center) -- cycle;
      \addplot[domain=-0.2:6.5,dashed,purple] {-2*x + 2};
      \addplot[domain=-0.2:6.5,dashed,purple] {-2*x + 4};
      \addplot[domain=-0.2:6.5,dashed,purple] {-2*x + 6};
      \addplot[domain=-0.2:6.5,dashed,purple] {-2*x + 8};
      \addplot[domain=-0.2:6.5,dashed,purple] {-2*x + 10};
      \draw[line width=2pt,teal,-stealth](0,0)--(-1,-0.5) node[anchor=south west]{$\boldsymbol{c}$};
      \draw[line width=2pt,purple,-stealth](0,0)--(1,0.5) node[anchor=south east]{$\boldsymbol{-c}$};
      \node[circle, red, fill=purple,scale=0.5] at (4,2) {};
    \end{axis}
  \end{tikzpicture}
\end{center}

Como, de fato, a solução ótima é dada por $x^T = \begin{bmatrix}4 & 2\end{bmatrix}$, temos a confirmação do resultado obtido pelo simplex revisado.

\section{Informações adicionais}

O arquivo \texttt{teste.m} contém uma bateria de testes automatizados (dos quais 3 foram representados na seção anterior). Para executa-los no octave, utilize:

\begin{verbatim}
octave> test teste
\end{verbatim}

\end{document}
