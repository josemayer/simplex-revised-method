function [bind Binv x] = pivot(A, bind, Binv, l, j, m, x, theta_star)
		% Computa se é limitado na j-ésima direcao escolhida
		u = Binv * A(:,j);

    % Substitui o indice da base
		for i = 1:m
			if i != l
				Binv(i, :) = Binv(i, :) - (u(i)/u(l)) * Binv(l, :);
			end
		end

		Binv(l, :) = Binv(l, :)/u(l);


		x(bind) = x(bind) - theta_star * u;
		x(j) = theta_star;

		bind(l) = j;
endfunction
