\documentclass{article}
\usepackage{xcolor}
\usepackage[utf8]{inputenc}
\usepackage{algpseudocode, algorithm,float}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{tikz}
\usepackage{pgfplots}

\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}
\pgfplotsset{compat=1.18}

% Declaracoes em Português
\algrenewcommand\algorithmicend{\textbf{fim}}
\algrenewcommand\algorithmicdo{\textbf{faça}}
\algrenewcommand\algorithmicwhile{\textbf{enquanto}}
\algrenewcommand\algorithmicfor{\textbf{para}}
\algrenewcommand\algorithmicforall{\textbf{para cada}}
\algrenewcommand\algorithmicif{\textbf{se}}
\algrenewcommand\algorithmicthen{\textbf{então}}
\algrenewcommand\algorithmicelse{\textbf{senão}}
\algrenewcommand\algorithmicreturn{\textbf{devolve}}
\algrenewcommand\algorithmicfunction{\textbf{função}}

% Renomeia a seção de algoritmo
\makeatletter
\renewcommand{\ALG@name}{Algoritmo}

% Rearranja os finais de cada estrutura
\algrenewtext{EndWhile}{\algorithmicend\ \algorithmicwhile}
\algrenewtext{EndFor}{\algorithmicend\ \algorithmicfor}
\algrenewtext{EndIf}{\algorithmicend\ \algorithmicif}
\algrenewtext{EndFunction}{\algorithmicend\ \algorithmicfunction}

\algnewcommand\algorithmicto{\textbf{até}}
\algrenewtext{For}[3]%
{\algorithmicfor\ #1 $\gets$ #2 \algorithmicto\ #3 \algorithmicdo}

% Comandos auxiliares para plotagem de solução gráfica
\usepgfplotslibrary{fillbetween}
\usetikzlibrary{patterns}

\makeatletter
\newcommand{\pgfplotsdrawaxis}{\pgfplots@draw@axis}
\makeatother
\pgfplotsset{only axis on top/.style={axis on top=false, after end axis/.code={
						 \pgfplotsset{axis line style=opaque, ticklabel style=opaque, tick style=opaque,
													grid=none}\pgfplotsdrawaxis}}}

\newcommand{\drawge}{-- (rel axis cs:1,0) -- (rel axis cs:1,1) -- (rel axis cs:0,1) \closedcycle}
\newcommand{\drawle}{-- (rel axis cs:1,1) -- (rel axis cs:1,0) -- (rel axis cs:0,0) \closedcycle}

% Possibilita espaçamento vertical na bmatrix
\makeatletter
\renewcommand*\env@matrix[1][\arraystretch]{%
	\edef\arraystretch{#1}%
	\hskip -\arraycolsep
	\let\@ifnextchar\new@ifnextchar
	\array{*\c@MaxMatrixCols c}}
\makeatother

\title{Otimização Linear (MAC0315) \\ Relatório - EP 3}
\author{José Lucas Silva Mayer \\ n.º USP: 11819208 \\ \\ Maximilian Cabrajac Goritz \\ n.º USP: 11795418}
\date{Dezembro, 2022.}

\begin{document}

\maketitle

\section{Introdução}

O método simplex é utilizado para resolver problemas de programação linear na forma padrão, ou seja, que podem ser definidos por uma matriz $A \in \mathbb{R}^{m \times n}$ e por vetores $b \in \mathbb{R}^m$ e $c \in \mathbb{R}^n$ onde $m, n \in \mathbb{Z}$ e $0 \leq m \leq n$, de forma que $Ax = b$ representa as restrições que não são da forma $x \geq 0$ e $c$ representa a função de custos $c^T x$.

Em sua forma completa, ele dispõe de duas fases principais: a primeira, responsável por encontrar uma solução viável básica (e, consequentemente, uma base) para o problema original a partir de um problema auxiliar; e a segunda, responsável por resolver o problema original dada a solução e a base encontradas na primeira fase.

Para esse exercício programa, implementamos o método simplex revisado de duas fases, o qual mantém, durante suas iterações, a inversa da matriz básica ($B^{-1}$) calculada.
Este algoritmo é mais rápido em relação a uma implementação ingênua, uma vez que não recalcula $B^{-1}$ a cada iteração.

Em anexo a este relatório, há o código-fonte, em octave, do método simplex revisado de duas fases \textminus no arquivo \texttt{simplex.m} \textminus, o código-fonte do iterador do simplex (desenvolvido no segundo exercício-programa) \textminus no arquivo \texttt{simplexIteration.m} \textminus, o código-fonte da função auxiliar de pivotamento\footnote{Função responsável pela atualização de $B^{-1}$, dos índices das variáveis básicas e da solução básica correspondente a partir da substituição da $j$-ésima coluna básica pela coluna $l$.} \textminus no arquivo \texttt{pivot.m} \textminus e uma bateria de testes \textminus no arquivo \texttt{teste.m}.

\newpage

\section{Problema auxiliar}

Para que o algoritmo da primeira fase do método simplex seja válido, é necessário, primeiro, que $b \geq 0$ (esta necessidade será justificada posteriormente).
Para isso, as restrições que possuem $b_{i} < 0$, $i \in [m]$ são multiplicadas por -1.

Na primeira fase do metodo simplex, criamos um problema auxiliar para que, por meio deste, seja possível encontrar uma solução viável básica inicial para o problema original.
Para isso, adicionam-se $m$ variáveis auxiliares $y_{1}, \ldots, y_{m}$ ao problema original, uma para cada restrição do problema original, e define-se uma nova função de custos que corresponda à soma dessas variáveis auxiliares.
Vejamos a criação de um problema auxiliar a partir de um original no formato padrão:

\begin{center}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{align*}
			\text{minimizar} \quad & c^{T}x \\
			\text{sujeito a} \quad & Ax = b \\
														 & x \geq 0
		\end{align*}
	\end{minipage}
	\hfill
	\begin{minipage}[t]{0.45\textwidth}
		\begin{align*}
			\text{minimizar} \quad & y_{1} + \cdots + y_{m} \\
			\text{sujeito a} \quad & Ax + y = b \\
														 & x \geq 0 \\
														 & y \geq 0
		\end{align*}
	\end{minipage}
\end{center}

Isso implica em considerar a nova matriz $A_{aux}$, formada pela concatenação da matriz $A$ com a matriz identidade de dimensão $m \times m$ ($A_{aux} = \begin{bmatrix} A & \mathcal{I}_{m} \end{bmatrix}$) e o novo vetor de custos $c_{aux}$, de tamanho $n + m$, que contém somente a soma das variáveis auxiliares ($c^{T} = \begin{bmatrix} 0_{1 \times n} & 1_{1 \times m} \end{bmatrix}$).

Notemos que o problema auxiliar, devido à exigência de que $b \geq 0$, sempre possui uma solução viável básica determinada por $\begin{bmatrix}x^T y^T\end{bmatrix} = \begin{bmatrix}0_{1 \times n} & b^T\end{bmatrix}$ associada à base formada pelas $m$ variaveis adicionais, com a matriz básica $B = \mathcal{I}_m = B^{-1}$.

Notemos, ainda, que existe solução viável para o problema original se e somente se o custo ótimo do problema auxiliar é zero, uma vez que, se o custo ótimo do problema auxiliar é estritamente maior que zero, isso implica que não existe solução viável para o problema tal que $y = 0$, ou seja, não existe $x \geq 0$ tal que $Ax = b$, resultando em um problema original inviável.

Por fim, notemos que é possível encontrar uma solução ótima para o problema auxiliar, em que o custo ótimo é zero, com variáveis auxiliares restantes na base.
Seja a $l$-ésima variável básica uma das variáveis auxiliares e $g$ a $l$-ésima linha de $B^{-1}$, conforme Bertsimas e Tsitsiklis\footnote{Bertsimas, D. and Tsitsiklis, J.N. (1997) \textbf{Introduction to Linear Optimization}. Athenas Scientific and Dynamic Ideas, Belmont. páginas 112-113.}. Isto pode acontecer de duas formas:

\begin{itemize}
	\item $gA \neq 0$:
		  Implica que existe $j$ tal que $gA_j$ é não-nulo e podemos substituir a variável auxiliar que permaneceu na base pela $j$-ésima variável.

	\item $gA = 0$:
		Implica que a $l$-ésima restrição de $A$ é redundante e esta restrição, em conjunto com a variável auxiliar correspondente, podem ser eliminadas. Neste caso, para que seja possível continuar o algoritmo, é necessário que exista uma nova matriz $\overline{B}^{-1}$ correspondente ao problema após estas eliminações.

		Sem perda de generalidade, suponha que $l = m$. Então, marcando com $\cdot$ as regiões da matriz que não são constantes nem relavantes para a argumentação, $B$ tem a forma dada:

		\begin{gather*}
			B = \begin{bmatrix}
				\overline{B} & 0\\
				\cdot & 1
			\end{bmatrix}
		\end{gather*}

		Sabemos, também, que $BB^{-1} = \mathcal{I}$:

		\begin{align*}
			BB^{-1} &= \mathcal{I}\\
			\begin{bmatrix}
				\overline{B} & 0\\
				\cdot & 1
			\end{bmatrix}\begin{bmatrix}
				X & \cdot\\
				\cdot & \cdot\\
			\end{bmatrix}&=\begin{bmatrix}
				\mathcal{I} & 0\\
				0 & 1
			\end{bmatrix}\\
			\overline{B}X &= \mathcal{I}
		\end{align*}

		Por meio do método descrito, o algoritmo é capaz de continuar, após extrair $\overline{B}^{-1}$ de $B^{-1}$.
\end{itemize}

Assim é finalizada a primeira fase do algoritmo simplex, resultando ou em uma solução viável básica para o problema original ou em um indicador de inviabilidade.

\section{Pseudocódigos}

A função $\Call{simplexIt}$ implementa as iterações da segunda fase do método simplex, assim como feito no exercício-programa anterior. Adicionalmente, essa nova função devolve $b_{ind}$ e $B^{-1}$ de acordo com o resultado obtido ao fim das iterações.

\begin{algorithm}[H]
\caption{Método simplex revisado de duas fases}
\begin{algorithmic}[1]
	\Function{Simplex}{$A$, $b$, $c$, $m$, $n$}
		\For {$i$} {1} {$m$}
			\If {$b_i < 0$}
				\State $A_i \gets -A_i$
				\State $b_i \gets -b_i$
			\EndIf
		\EndFor

		\State $A_{aux} \gets \begin{bmatrix}A & \mathcal{I}_{m}\end{bmatrix}$
		\State $c_{aux} \gets \begin{bmatrix}0_{(n \times 1)} \\ 1_{(m \times 1)}\end{bmatrix}$
		\State $B^{-1} \gets \begin{bmatrix}\mathcal{I}_{m}\end{bmatrix}$
		\State $b_{ind} \gets [n + 1,\dots, n + m]$
		\State $x \gets \begin{bmatrix}0_{n \times 1} \\ b\end{bmatrix}$


		\State $\left(\_, x^{*}, b_{inv}, B^{-1} \right) \gets \Call{simplexIt}{A_{aux}, b, c_{aux}, m, n + m, x, b_{ind}, B^{-1}}$

		\If {$(c_{aux})_{B}^{T}x^{*} > 0$}
			\State \Return (1, 0, 0) \Comment{Problema inviável}
		\EndIf

		\For {$l$} {1} {$m$}
			\If {$n+1 \leq b_{ind}[l] \leq n+m$}
				\State $g \gets l$-ésima linha de $B^{-1}$
				\If {$gA \neq 0$}
					\State \textbf{escolha} $j$ tal que $gA_j \neq 0$
					\State \textbf{faça} uma troca de base inserindo $j$ e removendo $B_{ind}[l]$
				\Else
					\State \textbf{marque} a $l$-ésima restrição como redundante
				\EndIf
			\EndIf

			\ForAll {restrição redundante}
				\State $\text{\textbf{remova} a restrição de $A$ e atualiza $b_{ind}$ e $B^{-1}$ adequadamente}$
			\EndFor

			\State $x \gets \text{primeiros $n$ elementos de $x^{*}$}$
			\State $(ind, v, \_, \_) \gets$ \Call{simplexIt}{$A$, $b$, $c$, $m$, $n$, $x$, $b_{ind}$, $B^{-1}$, }
			\If {$ind = 0$}
				\State \Return $(0, v, 0)$
			\Else
				\State \Return $(-1, 0, v)$
			\EndIf
		\EndFor
	\EndFunction
\end{algorithmic}
\end{algorithm}


\section{Funcionamento do Algoritmo em \texttt{simplex.m}}
O algoritmo implementado recebe, de ínicio, a matriz $A$ (\texttt{A}), o vetor $b$ (\texttt{b}), o vetor de custos $c$ (\texttt{c}) e as variáveis de dimensionalidade $m$ (\texttt{m}) e $n$ (\texttt{n}).

Para resolver um problema no formato padrão, o algoritmo do simplex revisado de duas fases (implementado no arquivo \texttt{simplex.m}) executa os seguintes passos:

\begin{itemize}
	\item Linha \texttt{3-8}: Altera $A$, em conjunto com $b$, para que $b$ se torne não-negativo.
	\item Linhas \texttt{10-15}: Cria uma matriz \texttt{A\_aux}, um vetor \texttt{c\_aux}, um vetor \texttt{bind} e uma matriz \texttt{Binv} que correspondam ao problema auxiliar utilizado para encontrar uma solução viável básica inicial para o problema.
	\item Linha \texttt{18}: Chama o iterador do simplex revisado para o problema auxiliar, recebendo o indicador \texttt{ind}, o valor de retorno \texttt{retv}, os índices da matriz básica \texttt{bind} e a inversa da matriz $B$ \texttt{Binv}.
	\item Linhas \texttt{22-29}: Verifica se o problema auxiliar tem custo maior que 0, caso em que não existe uma solução viável para o problema original. Se verdadeiro, devolve-o como inviável.
	\item Linhas \texttt{34-43}: Verifica se existe alguma variável auxiliar na base tal que $gA \neq 0$, assim como definido na seção 2. Se existir $j$ tal que $gA_j \neq 0$, chama-se a função \texttt{pivot}, que troca a $l$-ésima variável da base por $j$.
	\item Linhas \texttt{44-47}: Se existir uma variável auxiliar na base tal que $gA = 0$, marca-se a linha de $A$ como redundante.
	\item Linhas \texttt{51-52}: Retira todas as restrições redundantes de \texttt{A} e seus valores de \texttt{b} associados, eliminando as linhas correspondentes da matriz e do vetor, respectivamente.
	\item Linhas \texttt{53-55}: Atualiza \texttt{Binv} e \texttt{bind} para refletir as mudanças feitas em \texttt{A} e \texttt{b}, retirando todas as linhas e colunas dessa matriz marcadas como redundantes.
	\item Linha \texttt{56}: Atualiza a quantidade \texttt{m} de restrições do problema, devido às eliminações de restrições redundantes realizadas.
	\item Linhas \texttt{58-67}: Extrai a solução viável básica \texttt{x} e chama o iterador do simplex revisado para o problema original, recebendo o indicador \texttt{ind} e o valor de retorno \texttt{retv}. Tratamos estes valores para a saída correspondente do algoritmo do simplex.
\end{itemize}

\section{Testes}
% Função que facilita os teste:
\newcommand{\simplexOut}[5]{
\begin{center}
	\begin{tabular}{ccccc}
		\texttt{ind} = #1 &
		$x^* = #2$ &
		$c^Tx^* = #3$ &
		$B^{-1} = #4$ &
		$b_{ind} = #5$
	\end{tabular}
\end{center}
}
\newcommand{\mat}[1]{
	\begin{bmatrix}
		#1
	\end{bmatrix}
}

Para todos os testes, consideramos que as variáveis auxiliares do tipo $y_{1}, \cdots, y_{m}$ são representadas por $x_{n + 1}, \cdots, x_{n + m}$. Também consideramos que o algoritmo iterador do simplex (desenvolvido e simulado no exercício-programa 2) sempre retorna o resultado correto.

\subsection{Problema 1}

Seja o problema de programação linear definido por:

\begin{alignat*}{4}
		\text{minimizar } && && -x_{2} &&\\
		\text{sujeito a } && x_{1} && + x_{2} && = 5 \\
		&& x_{1},&&x_{2} &&\geq 0
\end{alignat*}

O problema está no formato padrão. Temos, para ele, a matriz $A = \begin{bmatrix}1 & 1\end{bmatrix}$, o vetor $b = \begin{bmatrix}5\end{bmatrix}$ e o vetor de custos $c^{T} = \begin{bmatrix}0 & -1\end{bmatrix}$.

\subsubsection{Fase I}

Vejamos o seu problema auxiliar:

\begin{alignat*}{5}
	\text{minimizar } && && && x_{3} &&\\
	\text{sujeito a } && x_{1} && + x_{2} && + x_{3} && = 5 \\
										&& x_{1},&& x_{2}, && x_{3} &&\geq 0
\end{alignat*}

Para o problema auxiliar, temos a matriz $A_{aux} = \begin{bmatrix} 1 & 1 & 1 \end{bmatrix}$, o vetor $b = \begin{bmatrix} 5 \end{bmatrix}$, o vetor de custos $c_{aux}^{T} = \begin{bmatrix} 0 & 0 & 1 \end{bmatrix}$, a matriz básica e sua inversa $B = B^{-1} = \mathcal{I}_1$ e o vetor de índices da base $b_{ind} = \begin{bmatrix} 3 \end{bmatrix}$.

Nesse caso, a base determina uma solução básica viável inicial para o problema auxiliar $x^{T} = \begin{bmatrix}0 & 0 & 5\end{bmatrix}$. Aplicando a iteração do método simplex, temos o seguinte resultado para a solução ótima do problema auxiliar:

	\simplexOut{0}{\mat{5\\0\\0}}{0}{\mat{1}}{\mat{1}}

Como o custo ótimo é 0, o problema original é viável. Além disso, como não existem, na base, colunas associadas às variáveis auxiliares, então apenas eliminamos as colunas de $x^{*}$ correspondentes a essas variáveis e criamos uma solução viável básica do problema original $x^{T} = \begin{bmatrix} 5 & 0 \end{bmatrix}$.

\subsubsection{Fase II}

Aplicando, agora, o iterador para $B^{-1}$ e $b_{ind}$ obtidas e para o $x$ construído, temos:

\simplexOut{0}{\mat{0\\5}}{-5}{\mat{1}}{\mat{2}}

Vejamos a representação e resolução gráfica desse problema, com a solução ótima marcada em vermelho e a solução inicial em roxo:

\begin{center}
	\begin{tikzpicture}
		\begin{axis}[smooth,
			axis line style=very thick,
			axis x line=bottom,
			axis y line=left,
			grid=major,
			grid style={line width=.1pt, draw=gray!30},
			ymin=-1.0,ymax=5.5,xmin=-0.2,xmax=5.5,
			xlabel=$x_1$, ylabel=$x_2$,
			xtick={0, 1, 2, 3, 4, 5},
			ytick={0, 1, 2, 3, 4, 5},
			axis lines=middle]
			\addplot[thick,domain=-0.2:5.5] {5 - x};
			\node (a) at (0, 5) {};
			\node (b) at (5, 0) {};
			\addplot[domain=-0.2:5.5,dashed,purple] {5};
			\addplot[domain=-0.2:5.5,dashed,purple] {3};
			\addplot[domain=-0.2:5.5,dashed,purple] {1};
			\node[circle, red, fill=purple,scale=0.5] at (0, 5) {};
			\node[circle, yellow, fill=violet,scale=0.5] at (5, 0) {};
			\draw[line width=2pt,teal,-stealth](0,0)--(0,-1) node[anchor=south west]{$\boldsymbol{c}$};
			\draw[line width=2pt,purple,-stealth](0,0)--(0,1) node[anchor=north west]{$\boldsymbol{-c}$};
		\end{axis}
	\end{tikzpicture}
\end{center}

É possível notar que o vértice inicial $x^{T} = \begin{bmatrix} 5 & 0 \end{bmatrix}$ é, de fato, uma solução viável básica do problema. Ademais, como a solução ótima é $(x^{*})^{T} = \begin{bmatrix}0 & 5\end{bmatrix}$, temos a confirmação do resultado obtido pelo simplex revisado de duas fases.

\subsection{Problema 2}

Seja o problema de programação linear definido por:


\begin{alignat*}{4}
		\text{minimizar } && x_{1} && + x_{2} &&\\
		\text{sujeito a } && x_{1} && + 10x_{2} && = 10 \\
											&& 2x_{1} && + 5x_{2} && = 1 \\
											&& x_{1}, && x_{2} && \geq 0
\end{alignat*}

O problema está no formato padrão. A partir deste temos a matriz $A = \begin{bmatrix}1 & 10 \\ 2 & 5\end{bmatrix}$, o vetor $b = \begin{bmatrix}10 \\ 1\end{bmatrix}$ e o vetor de custos $c^{T} = \begin{bmatrix}1 & 1\end{bmatrix}$.

\subsubsection{Fase I}

Vejamos o seu problema auxiliar:

\begin{alignat*}{6}
	\text{minimizar } && && && x_{3} && + x_{4} &&\\
	\text{sujeito a } && x_{1} && + 10x_{2} && + x_{3} && && = 10 \\
										&& 2x_{1} && + 5x_{2} && && + x_{4} && = 1 \\
										&& x_{1},&& x_{2}, && x_{3}, && x_{4} &&\geq 0
\end{alignat*}

Para o problema auxiliar, temos a matriz $A_{aux} = \begin{bmatrix} 1 & 10 & 1 & 0 \\ 2 & 5 & 0 & 1 \end{bmatrix}$, o vetor $b = \begin{bmatrix} 10 \\ 1 \end{bmatrix}$, o vetor de custos $c_{aux}^{T} = \begin{bmatrix} 0 & 0 & 1 & 1 \end{bmatrix}$, a matriz básica e sua inversa $B = B^{-1}= \mathcal{I}_2$ e o vetor de índices da base $b_{ind} = \begin{bmatrix} 3 & 4 \end{bmatrix}$.

Nesse caso, a base determina uma solução básica viável inicial para o problema auxiliar $x^{T} = \begin{bmatrix}0 & 0 & 10 & 1\end{bmatrix}$. Aplicando a iteração do método simplex, temos o seguinte resultado para a solução ótima do problema auxiliar:

\simplexOut{0}{\mat{0\\\frac{1}{5}\\8\\0}}{8}{\mat{1 & -2\\0 & \frac{1}{2}}}{\mat{3 & 2}}

Como o custo ótimo é 8, que é maior que 0, o problema original é inviável e não pode ter uma solução viável básica inicial.

Vejamos a representação gráfica desse problema:

\begin{center}
	\begin{tikzpicture}
		\begin{axis}[smooth,
			axis line style=very thick,
			axis x line=bottom,
			axis y line=left,
			grid=major,
			grid style={line width=.1pt, draw=gray!30},
			ymin=-1,ymax=3.5,xmin=-3.5,xmax=3.5,
			xlabel=$x_1$, ylabel=$x_2$,
			xtick={-3, -1, -2, 0, 1, 2, 3},
			ytick={0, 1, 2, 3},
			axis lines=middle,
			width=10cm,
			height=6.5cm]
			\addplot[thick,domain=-3.5:3.5] {-x/10 + 1};
			\addplot[thick,domain=-3.5:3.5] {-2/5*x + 1/5};
			\addplot[domain=-2:1,dashed,purple] {-x - 2/5};
			\addplot[domain=-3:1,dashed,purple] {-x - 7/5};
			\draw[line width=2pt,teal,-stealth](0,0)--(0.4,0.4) node[anchor=south west]{$\boldsymbol{c}$};
			\draw[line width=2pt,purple,-stealth](0,0)--(-0.4,-0.4) node[anchor=south east]{$\boldsymbol{-c}$};
		\end{axis}
	\end{tikzpicture}
\end{center}

De fato o problema é inviável, uma vez que o único ponto onde as duas restrições de igualdade são ativas pertence ao segundo quadrante, não satisfazendo, assim, as restrições de desigualdade.

\subsection{Problema 3}

Seja o problema de programação linear definido por:

\begin{alignat*}{6}
		\text{minimizar } && x_{1} && + x_{2} && + x_{3} && &&\\
		\text{sujeito a } && x_{1} && + 2x_{2} && + 3x_{3} && && = 3 \\
											&& -x_{1} && + 2x_{2} && +6x_{3} && && = 2 \\
											&& && + 4x_{2} && + 9x_{3} && && = 5 \\
											&& && && +3x_{3} && + x_{4} && = 1 \\
											&& 2x_{1} && + 4x_{2} && + 6x_{3} && && = 6 \\
											&& x_{1}, && x_{2}, && x_{3}, && x_{4} &&\geq 0
\end{alignat*}

O problema está no formato padrão. Temos, para ele, os valores de:

\begin{center}
	\begin{tabular}{ccc}
		$A = \begin{bmatrix}1 & 2 & 3 & 0 \\ -1 & 2 & 6 & 0 \\ 0 & 4 & 9 & 0 \\ 0 & 0 & 3 & 1 \\ 2 & 4 & 6 & 0 \end{bmatrix}$ &
		$b = \begin{bmatrix} 3 \\ 2 \\ 5 \\ 1 \\ 6 \end{bmatrix}$ &
		$c = \begin{bmatrix} 1 \\ 1 \\ 1 \\ 0 \end{bmatrix}$
	\end{tabular}
\end{center}

\subsubsection{Fase I}

Vejamos o problema auxiliar:

\begin{alignat*}{11}
	\text{minimizar } && && && && && x_{5} && + x_{6} && + x_{7} && + x_{8} && + x_{9} && \\
	\text{sujeito a } && x_{1} && + 2x_{2} && + 3x_{3} && && + x_{5} && && && && && = 3 \\
										&& -x_{1} && + 2x_{2} && +6x_{3} && && && + x_{6} && && && && = 2 \\
										&& && + 4x_{2} && + 9x_{3} && && && && + x_{7} && && && = 5 \\
										&& && && +3x_{3} && + x_{4} && && && && + x_{8} && && = 1 \\
										&& 2x_{1} && + 4x_{2} && + 6x_{3} && && && && && && + x_{9} && = 6 \\
										&& x_{1}, && x_{2}, && x_{3}, && x_{4}, && x_{5}, && x_{6}, && x_{7}, && x_{8}, && x_{9} && \geq 0
\end{alignat*}

Para o problema auxiliar, temos:

\begin{center}
	\begin{tabular}{ccc}
		$A_{aux} = \begin{bmatrix}1 & 2 & 3 & 0 & 1 & 0 & 0 & 0 & 0 \\ -1 & 2 & 6 & 0 & 0 & 1 & 0 & 0 & 0 \\ 0 & 4 & 9 & 0 & 0 & 0 & 1 & 0 & 0 \\ 0 & 0 & 3 & 1 & 0 & 0 & 0 & 1 & 0 \\ 2 & 4 & 6 & 0 & 0 & 0 & 0 & 0 & 1 \end{bmatrix}$ &
		$b = \begin{bmatrix} 3 \\ 2 \\ 5 \\ 1 \\ 6 \end{bmatrix}$ &
		$c_{aux} = \begin{bmatrix} 0 \\ 0 \\ 0 \\ 0 \\ 1 \\ 1 \\ 1 \\ 1 \\ 1 \end{bmatrix}$\\
	\end{tabular}
	\\
	\begin{tabular}{cc}
		$B = B^{-1} = \mathcal{I}_5$ &
			$b_{ind} = \begin{bmatrix} 5 & 6 & 7 & 8 & 9 \end{bmatrix}$
	\end{tabular}
\end{center}


Nesse caso, a base determina uma solução básica viável inicial para o problema auxiliar $x^{T} = \begin{bmatrix}0 & 0 & 0 & 0 & 3 & 2 & 5 & 1 & 6\end{bmatrix}$. Aplicando a iteração do método simplex, temos o seguinte resultado para a solução ótima do problema auxiliar:

\begin{center}
	\begin{tabular}{ccc}
		\texttt{ind} = 0 &
		$x^* = \mat{1\\1/2\\1/3\\0\\0\\0\\0\\0\\0}$ &
		$c_{aux}^Tx^* = 0$\\
		$B^{-1} = \begin{bmatrix}[1.5] \frac{1}{2} & -\frac{1}{2} & 0 & \frac{1}{2} & 0 \\ \frac{1}{4} & \frac{1}{4} & 0 & -\frac{3}{4} & 0 \\ -1 & -1 & 1 & 0 & 0 \\ 0 & 0 & 0 & \frac{1}{3} & 0 \\ -2 & 0 & 0 & 0 & 1 \end{bmatrix}$ &
			$b_{ind} = \mat{1&2&7&3&9}$
	\end{tabular}
\end{center}


Como o custo ótimo é 0, o problema original é viável. No entanto, existem, na base, índices associados às variáveis auxiliares 7 e 9, como mostrado por $b_{ind}$. Notemos que 7 e 9 são, respectivamente, a terceira e quinta coluna da matriz básica. Para esses dois índices, então, calculamos a respectiva linha do algoritmo do \textit{tableau}, multiplicando, para $l \in \{3, 5\}$, a $l$-ésima linha de $B^{-1}$ por $A$. Seja $g_{l}$ a notação para a $l$-ésima linha de $B^{-1}$, temos:

\begin{align*}
	g_{3} A & = \begin{bmatrix} -1 & -1 & 1 & 0 & 0 \end{bmatrix}
	\begin{bmatrix} 1 & 2 & 3 & 0 \\ -1 & 2 & 6 & 0 \\ 0 & 4 & 9 & 0 \\ 0 & 0 & 3 & 1 \\ 2 & 4 & 6 & 0 \end{bmatrix} \\
							 & = \begin{bmatrix} 0 & 0 & 0 & 0 \end{bmatrix}
\end{align*}

\begin{align*}
	g_{5} A & = \begin{bmatrix} -2 & 0 & 0 & 0 & 1 \end{bmatrix}
	\begin{bmatrix} 1 & 2 & 3 & 0 \\ -1 & 2 & 6 & 0 \\ 0 & 4 & 9 & 0 \\ 0 & 0 & 3 & 1 \\ 2 & 4 & 6 & 0 \end{bmatrix} \\
							 & = \begin{bmatrix} 0 & 0 & 0 & 0 \end{bmatrix}
\end{align*}

Como encontramos duas linhas que têm unicamente valores iguais a 0, isso implica que as restrições 3 e 5 são linearmente dependentes em relação a outras restrições do problema e podem ser eliminadas. Tomando $a_{i}$ como a $i$-ésima linha de $A$, de fato, podemos observar que $a_{3} = a_{1} + a_{2}$ e $a_{5} = 2a_{1}$. No programa, $a_{3}$ e $a_{5}$ são marcadas como redundantes e, então, eliminadas, de modo que temos os novos valores de $A$ e $b$ para o problema original:

\begin{center}
	\begin{tabular}{ccc}
		$A = \begin{bmatrix}1 & 2 & 3 & 0 \\ -1 & 2 & 6 & 0 \\ 0 & 0 & 3 & 1 \end{bmatrix}$&
		$b = \begin{bmatrix} 3 \\ 2 \\ 1 \end{bmatrix}$ &
		$c = \begin{bmatrix} 1 \\ 1 \\ 1 \\ 0 \end{bmatrix}$
	\end{tabular}
\end{center}


Além disso, atualizamos $B^{-1}$ e $b_{ind}$ encontrados na fase 1 de forma correspondente:

\begin{center}
	\begin{tabular}{cc}
		$B^{-1} = \begin{bmatrix}[1.5] \frac{1}{2} & -\frac{1}{2} & \frac{1}{2} \\ \frac{1}{4} & \frac{1}{4} & -\frac{3}{4} \\ 0 & 0 & \frac{1}{3} \end{bmatrix}$ &
		$b_{ind} = \begin{bmatrix} 1 & 2 & 3 \end{bmatrix}$
	\end{tabular}
\end{center}

Agora, como a base possui apenas índices associados às variáveis do problema original, podemos eliminar os índices relacionados às variáveis auxiliares e formar uma solução viável básica inicial $x^{T} = \begin{bmatrix}1 & \frac{1}{2} & \frac{1}{3} & 0 \end{bmatrix}$.

\subsubsection{Fase II}

Aplicando o iterador do simplex para a solução viável básica inicial $x^{T}$, para o vetor de custos $c$ e para os novos valores de $A$, $b$, $B^{-1}$ e $b_{ind}$, obtemos:

\simplexOut{0}{\mat{1/2\\5/4\\0\\1}}{\frac{7}{4}}{\begin{bmatrix}[1.5] \frac{1}{2} & -\frac{1}{2} & 0 \\ \frac{1}{4} & \frac{1}{4} & 0 \\ 0 & 0 & 1 \end{bmatrix}}{[1, 2, 4]}

Esse problema, sem a inclusão da última restrição (que é redundante em relação às demais), é solucionado pelo método do $M$-grande no exemplo 3.9 de Bertsimas e Tsitsiklis. De fato, pela solução, o custo ótimo é igual a $\frac{7}{4}$ e a solução viável básica ótima encontrada é $(x^{*})^{T} = \begin{bmatrix} \frac{1}{2} & \frac{5}{4} & 0 & 1 \end{bmatrix}$, indicando que o simplex revisado de duas fases encontra o vértice ótimo para o problema original.

\subsection{Problema 4}

Seja o problema de programação linear definido por:

\begin{alignat*}{7}
		\text{minimizar } && 2x_{1} && + 3x_{2} && + 3x_{3} && + x_{4} && -2x_{5} &&\\
		\text{sujeito a } && x_{1} && + 3x_{2} && && + 4x_{4} && + x_{5} && = 2 \\
											&& x_{1} && + 2x_{2} && && -3x_{4} && + x_{5} && = 2 \\
											&& -x_{1} && - 4x_{2} && + 3x_{3} && && && = 1 \\
											&& x_{1}, && x_{2}, && x_{3}, && x_{4}, && x_{5} &&\geq 0
\end{alignat*}

O problema está no formato padrão. Temos, para ele, os valores de:

\begin{center}
	\begin{tabular}{ccc}
		$A = \begin{bmatrix}1 & 3 & 0 & 4 & 1 \\ 1 & 2 & 0 & -3 & 1 \\ -1 & -4 & 3 & 0 & 0 \end{bmatrix}$ &
		$b = \begin{bmatrix} 2 \\ 2 \\ 1 \end{bmatrix}$ &
		$c = \begin{bmatrix} 2 \\ 3 \\ 3 \\ 1 \\ -2 \end{bmatrix}$
	\end{tabular}
\end{center}


\subsubsection{Fase I}

Vejamos o problema auxiliar:

\begin{alignat*}{10}
		\text{minimizar } && && && && && && x_{6} && + x_{7} && + x_{8} &&\\
		\text{sujeito a } && x_{1} && + 3x_{2} && && + 4x_{4} && + x_{5} && + x_{6} && && && = 2 \\
											&& x_{1} && + 2x_{2} && && -3x_{4} && + x_{5} && && + x_{7} && && = 2 \\
											&& -x_{1} && - 4x_{2} && +3x_{3} && && && && && + x_{8} && = 1 \\
											&& x_{1}, && x_{2}, && x_{3}, && x_{4}, && x_{5}, && x_{6}, && x_{7}, && x_{8} && \geq 0
\end{alignat*}

Para o problema auxiliar, temos:

\begin{center}
	\begin{tabular}{ccc}
		$A_{aux} = \begin{bmatrix}1 & 3 & 0 & 4 & 1 & 1 & 0 & 0 \\ 1 & 2 & 0 & -3 & 1 & 0 & 1 & 0 \\ -1 & -4 & 3 & 0 & 0 & 0 & 0 & 1 \end{bmatrix}$ &
		$b = \begin{bmatrix} 2 \\ 2 \\ 1 \end{bmatrix}$ &
		$c = \begin{bmatrix} 0 \\ 0 \\ 0 \\ 0 \\ 0 \\ 1 \\ 1 \\ 1 \end{bmatrix}$
	\end{tabular}
\end{center}
\begin{center}
	\begin{tabular}{cc}
		$B = B^{-1} = \mathcal{I}_3$ &
		$b_{ind} = \begin{bmatrix} 6 & 7 & 8 \end{bmatrix}$
	\end{tabular}
\end{center}

Nesse caso, a base determina uma solução básica viável inicial para o problema auxiliar $x^{T} = \begin{bmatrix}0 & 0 & 0 & 0 & 0 & 2 & 2 & 1 \end{bmatrix}$. Aplicando a iteração do método simplex, temos o seguinte resultado para a solução ótima do problema auxiliar:

	\simplexOut{0}{\begin{bmatrix}2 \\ 0 \\ 1 \\ 0 \\ 0 \\ 0 \\ 0 \\ 0 \\ 0\end{bmatrix}}{0}{\begin{bmatrix} 1 & 0 & 0 \\ -1 & 1 & 0 \\ \frac{1}{3} & 0 & \frac{1}{3} \end{bmatrix}}{\begin{bmatrix} 1 & 7 & 3 \end{bmatrix}}

Como o custo ótimo é 0, o problema original é viável. No entanto, existe, na base, o índice associado à variável auxiliar 7, como mostrado por $b_{ind}$. Notemos que 7 é a segunda coluna da matriz básica. Para esse índice, então, calculamos a respectiva linha do algoritmo do \textit{tableau}, multiplicando a segunda linha de $B^{-1}$ por $A$. Seja $g_{l}$ a notação para a $l$-ésima linha de $B^{-1}$, temos:

\begin{align*}
	g_{2} A & = \begin{bmatrix} -1 & 1 & 0 \end{bmatrix}
	\begin{bmatrix}1 & 3 & 0 & 4 & 1 \\ 1 & 2 & 0 & -3 & 1 \\ -1 & -4 & 3 & 0 & 0 \end{bmatrix} \\
 	 & = \begin{bmatrix} 0 & -1 & 0 & -7	& 0 \end{bmatrix}
\end{align*}

Como encontramos uma linha que tem valores não nulos, isso implica que precisamos escolher um desses valores como pivô para tirar o índice 7 da base. O algoritmo escolhe o primeiro valor, que, nesse caso, é -1, cujo índice correspondente é 2.

Para essa situação, precisamos realizar o processo de substituição da coluna 7 pela coluna 2 na base, atribuindo $b_{ind} = \begin{bmatrix} 1 & 2 & 3 \end{bmatrix}$ e modificando $B^{-1}$ de forma correspondente. Assim, de acordo com o método já explicado no exercício-programa 2, vamos alterar a matriz básica inversa para refletir essa substituição, procedimento que, aqui, chamamos de ``pivotagem":

Primeiro, calculamos $u = B^{-1}(A_{aux})_{2}$

\begin{align*}
	u & = \begin{bmatrix}[1.5] 1 & 0 & 0 \\ -1 & 1 & 0 \\ \frac{1}{3} & 0 & \frac{1}{3} \end{bmatrix}
	\begin{bmatrix}[1.5] 3 \\ 2 \\ -4 \end{bmatrix} \\
	& = \begin{bmatrix}[1.5] 3 \\ -1 \\ -\frac{1}{3} \end{bmatrix}
\end{align*}

Agora atualizamos $B^{-1}$, para o índice $l = 2$ escolhido para entrar na base:

\begin{align*}
	B^{-1} & = \begin{bmatrix}[1.5] 1 - \frac{u_{1}}{u_{2}}B^{-1}_{21} & 0 - \frac{u_{1}}{u_{2}}B^{-1}_{22} & 0 - \frac{u_{1}}{u_{2}}B^{-1}_{23} \\ -\frac{1}{u_{2}} & \frac{1}{u_{2}} & \frac{0}{u_{2}} \\ \frac{1}{3} - \frac{u_{3}}{u_{2}}B^{-1}_{21} & 0 - \frac{u_{3}}{u_{2}}B^{-1}_{22} & \frac{1}{3} - \frac{u_{3}}{u_{2}}B^{-1}_{23} \end{bmatrix} \\ 
	& = \begin{bmatrix}[2] 1 - \frac{3}{(-1)} \cdot (-1) & 0 - \frac{3}{(-1)} \cdot 1 & 0 - \frac{3}{(-1)} \cdot 0 \\ -\frac{1}{(-1)} & \frac{1}{(-1)} & \frac{0}{(-1)} \\ \frac{1}{3} - \frac{-\frac{1}{3}}{(-1)} \cdot (-1) & 0 - \frac{-\frac{1}{3}}{(-1)} \cdot 1 & \frac{1}{3} - \frac{-\frac{1}{3}}{(-1)} \cdot 0\end{bmatrix} \\
	& = \begin{bmatrix}[1.5] -2 & 3 & 0 \\ 1 & -1 & 0 \\ \frac{2}{3} & -\frac{1}{3} & \frac{1}{3}\end{bmatrix}
\end{align*}

Agora, como a base possui apenas índices associados às variáveis do problema original, podemos eliminar os índices relacionados às variáveis auxiliares e formar uma solução viável básica inicial $x^{T} = \begin{bmatrix}2 & 0 & 1 & 0 & 0 \end{bmatrix}$.

\subsubsection{Fase II}

Aplicando o iterador do simplex para a solução viável básica inicial $x^{T}$, para o vetor de custos $c$, para a matriz $A$, para o vetor $b$ e para os novos valores de $B^{-1}$ e $b_{ind}$, obtemos:

\simplexOut{0}{\mat{0\\0\\1/3\\0\\2}}{-3}{\begin{bmatrix}[1.5] \frac{3}{7} & \frac{4}{7} & 0 \\ \frac{1}{7} & -\frac{1}{7} & 0 \\ 0 & 0 & \frac{1}{3} \end{bmatrix}}{[5, 4, 3]}

Esse problema corresponde ao exercício 3.17 de Bertsimas e Tsitsiklis. Para verificar se a solução está correta, vamos resolvê-lo por meio do método do \textit{tableau} de duas fases:

Para encontrar uma solução viável básica inicial, temos o \textit{tableau} inicial para o problema auxiliar:

\begin{table}[H]
	\centering
	\begin{tabular}{|c|c|cccccccc|}
		\hline
						& -5 & -1 & -1 & -3 & -1 & -2 & 0 & 0 & 0 \\
		\hline
		$x_{6}$ & 2 & $1^{*}$ & 3 & 0 & 4 & 1 & 1 & 0 & 0 \\
		$x_{7}$ & 2 & 1 & 2 & 0 & -3 & 1 & 0 & 1 & 0 \\
		$x_{8}$ & 1 & -1 & -4 & 3 & 0 & 0 & 0 & 0 & 1 \\
		\hline

	\end{tabular}
\end{table}

Marcando os pivôs com $*$ e realizando as iterações, obtemos:

\begin{table}[H]
	\centering
	\begin{tabular}{|c|c|cccccccc|}
		\hline
						& -3 & 0 & 2 & -3 & 3 & -1 & 1 & 0 & 0 \\
		\hline
		$x_{1}$ & 2 & 1 & 3 & 0 & 4 & 1 & 1 & 0 & 0 \\
		$x_{7}$ & 0 & 0 & -1 & 0 & -7 & 0 & -1 & 1 & 0 \\
		$x_{8}$ & 3 & 0 & -1 & $3^{*}$ & 4 & 1 & 1 & 0 & 1 \\
		\hline

	\end{tabular}
\end{table}

\begin{table}[H]
	\centering
	\begin{tabular}{|c|c|cccccccc|}
		\hline
						& 0 & 0 & 1 & 0 & 7 & 0 & 2 & 0 & 0 \\
		\hline
		$x_{1}$ & 2 & 1 & 3 & 0 & 4 & 1 & 1 & 0 & 0 \\
		$x_{7}$ & 0 & 0 & $-1^{*}$ & 0 & -7 & 0 & -1 & 1 & 0 \\
		$x_{3}$ & 1 & 0 & $-\frac{1}{3}$ & 1 & $\frac{4}{3}$ & $\frac{1}{3}$ & $\frac{1}{3}$ & 0 & $\frac{1}{3}$ \\
		\hline

	\end{tabular}
\end{table}

Apesar do custo ser 0, notamos, do \textit{tableau} acima, que $x_{7}$ ainda é uma variável básica. Vamos executar o processo de substituição dessa variável, escolhendo o primeiro elemento não-nulo como pivô:

\begin{table}[H]
	\centering
	\begin{tabular}{|c|c|cccccccc|}
		\hline
						& 0 & 0 & 0 & 0 & 0 & 0 & 1 & 1 & 0 \\
		\hline
		$x_{1}$ & 2 & 1 & 0 & 0 & -17 & 1 & -2 & 3 & 0 \\
		$x_{2}$ & 0 & 0 & 1 & 0 & 7 & 0 & 1 & -1 & 0 \\
		$x_{3}$ & 1 & 0 & 0 & 1 & $\frac{11}{3}$ & $\frac{1}{3}$ & $\frac{2}{3}$ & $-\frac{1}{3}$ & $\frac{1}{3}$ \\
		\hline

	\end{tabular}
\end{table}

Agora, com a solução básica viável inicial $x^{T} = \begin{bmatrix} 2 & 0 & 1 & 0 & 0 \end{bmatrix}$ podemos aplicar, novamente, o algoritmo do \textit{tableau} para o problema inicial, apenas eliminando as três últimas colunas do último \textit{tableau} e recalculando a primeira linha de acordo, agora, com o problema original:

\begin{table}[H]
	\centering
	\begin{tabular}{|c|c|ccccc|}
		\hline
						& -7 & 0 & 0 & 0 & 3 & -5 \\
		\hline
		$x_{1}$ & 2 & 1 & 0 & 0 & -17 & $1^{*}$ \\
		$x_{2}$ & 0 & 0 & 1 & 0 & 7 & 0 \\
		$x_{3}$ & 1 & 0 & 0 & 1 & $\frac{11}{3}$ & $\frac{1}{3}$ \\
		\hline

	\end{tabular}
\end{table}

\begin{table}[H]
	\centering
	\begin{tabular}{|c|c|ccccc|}
		\hline
						& 3 & 5 & 0 & 0 & -82 & 0 \\
		\hline
		$x_{5}$ & 2 & 1 & 0 & 0 & -17 & 1 \\
		$x_{2}$ & 0 & 0 & 1 & 0 & $7^{*}$ & 0 \\
		$x_{3}$ & $\frac{1}{3}$ & $-\frac{1}{3}$ & 0 & 1 & $\frac{28}{3}$ & 0 \\
		\hline

	\end{tabular}
\end{table}

\begin{table}[H]
	\centering
	\begin{tabular}{|c|c|ccccc|}
		\hline
						& 3 & 5 & $\frac{82}{7}$ & 0 & 0 & 0 \\
		\hline
		$x_{5}$ & 2 & 1 & $\frac{17}{7}$ & 0 & 0 & 1 \\
		$x_{4}$ & 0 & 0 & $\frac{1}{7}$ & 0 & 1 & 0 \\
		$x_{3}$ & $\frac{1}{3}$ & $-\frac{1}{3}$ & $-\frac{4}{3}$ & 1 & 0 & 0 \\
		\hline

	\end{tabular}
\end{table}

Como não existem mais custos reduzidos negativos, então a solução encontrada pelo \textit{tableau} $(x^{*})^{T} = \begin{bmatrix}0 & 0 & \frac{1}{3} & 0 & 2\end{bmatrix}$ é ótima, com custo ótimo $c^{T}x^{*} = -3$. Como os vértices ótimos coincidem no simplex revisado de duas fases e no método do \textit{tableau}, concluímos que o algoritmo implementado chegou a uma solução correta para o problema.

\section{Informações adicionais}

O arquivo \texttt{teste.m} contém uma bateria de testes automatizados (dos quais 4 foram representados e simulados na seção anterior). Alguns testes foram reaproveitados do segundo exercício-programa, apenas considerando, agora, os valores de $A$, $b$, $c$, $m$ e $n$. Para executá-los no octave, utilize:

\begin{verbatim}
octave> test teste
\end{verbatim}

\end{document}
