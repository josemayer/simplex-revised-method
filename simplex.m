function [ind x d] = simplex(A, b, c, m, n)
  % Iterarar por b trocando o sinal das linhas que tem b_i < 0
  for i = 1:m
    if b(i) < 0
      b(i) *= -1;
      A(i,:) *= -1;
    endif
  endfor

  A_aux = horzcat(A, eye(m));
  c_aux = vertcat(zeros(n, 1), ones(m, 1));
  Binv = eye(m);
  bind = (n + 1):(n + m);
  x = zeros(n + m, 1);
  x(bind) = b;

  printf("Simplex: Fase 1\n\n");
  [retind retv bind Binv] = simplexIteration(A_aux, b, c_aux, m, n + m, x, bind, Binv);

  assert(retind == 0)

  aux_cost = transpose(c_aux) * retv;

  if aux_cost > 0
    ind = 1;
    x = 0;
    d = 0;
    return
  endif

  redundants = [];
  for l = 1:m
    if bind(l) >= n + 1 && bind(l) <= n + m
      pivoted = false;
      tab_row = Binv(l,:) * A;
      for j = 1:n
        if tab_row(j) != 0
          [bind, Binv] = pivot(A_aux, bind, Binv, l, j, m, x, 0);
          pivoted = true;
          break
        endif
      endfor
      if pivoted == false 
        % Marcar l-ésima restrição como redundante
        redundants = [redundants l];
      endif
    endif
  endfor

  % Retirar as variáveis redundantes
  A(redundants, :) = [];
  b(redundants, :) = [];
  bind(redundants) = [];
  Binv(redundants, :) = [];
  Binv(:, redundants) = [];
  m = m - length(redundants);

  printf("Simplex: Fase 2\n\n");
  x = retv(1:n);
  [ind, v, _, _] = simplexIteration(A, b, c, m, n, x, bind, Binv);
  if ind == 0
    x = v;
    d = 0;
  else
    x = 0;
    d = v;
  endif
endfunction
